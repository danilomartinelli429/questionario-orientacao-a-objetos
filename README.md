# Questionario Orientacao a Objetos

## Questão 1

    São os tipos mais básicos em que um computador pode armazenar variáveis, a partir destes vários objetos podem
    ser criados
    
    
## Questão 2

    Variáveis são responsáveis por armazenar referências a valores armazenados em memória RAM. 
    Declarar uma variável é alocar um espaço em memória e guardar a referência a esse espaço.
    Exemplo: int x = 2;
 
    
## Questão 3

    Constantes nomeadas são variáveis que tem um valor atruido e que nunca é alterado, ou seja, é imutável.
    A imutalidade, saber que a constante em uso nunca irá ter seu valor alterado. Tudo em maísculo com palavras
    separadas com "_".
    
## Questão 4

    Utilizar de uma expressão que caso verdadeiro, retorne algo, caso não seja verdadeiro, retorne outra coisa.
    isTrue ? retorneIsso : retorneOutraCoisa
    
## Questão 5

    Resposta: 8
    
## Questão 6

    Mudar o tipo de uma variável para outra. É implicita quando o algoritimo internamente faz isso, sem
    apresentar erros. Já explicitamente é necessário fazer o cast, ou seja, explecitar para qual tipo
    você está mudando.
    
## Questão 7

    São estruturas de repetição, ou seja, repetem um determinado código até que a condição seja
    satisfeita. "while" repete o código enquanto o parâmetro seja verdadeiro, "do while" executa
    o código e verifica a condição logo depois e o "for" em sua assinatura inicializa a variável,
    testa a condição e faz a incremenção.
    
## Questão 8

    São estruturas de condição, ou seja, executa determinado cógido se a condição for satisfeita,
    se não for, pode executar outro código. O break é necessário no switch pois caso ela caia em
    uma condição, se não utilizar o break para sair, ele passa por todas as outras mesmo que a
    condição dos outros não tenha sido satisfeita.
    
## Questão 9

    O "break" dentro de um laço interrompe sua execução mesmo que ainda esteja dentro das
    condições já o "continue" interrompe a execução de determinado laço e passa para o
    próximo laço do loop.
    
## Questão 10

    É um padrão de projeto, um dos paradigmas existentes na programação, seguido por
    java, C# e outros. Que define coisas como objeto, classe, herança, e.t.c.
    
## Questão 11

    A vantagem é que a repetição de código é menor e também a uma melhor organização do código,
    onde a estrutura do código é separada em classes, no caso, seguindo uma determinada semantica. 
    Dessa forma é mais fácil para trabalhar em grupo, pois não precisa saber o que acontece em
    todo o código para fazer determinada feature.
    
## Questão 12

    São instâncias de classes.
    
## Questão 13

    A classe é um conjunto de instruções e propiedades que definem um objeto.

## Questão 14

    Abstração é definir entidades reais em classes distintas.    

## Questão 15

    Encapsulamento é a ação de juntar propiedades similares em classes e separar bem as
    informações de forma que fique apenas o necessário acessavel para externos.

## Questão 16

    São propiedades de classes. O estado de um objeto é definido pelos seus atributos.

## Questão 17

    Sim, é possível. Seria prioritário a variável local. É possível forçar o acesso ao
    atributo da classe atráves da palavra reservada "this".

## Questão 18

    São instâncias de funções de uma determinada classe.

## Questão 19

    Nullo é uma forma de referenciar que uma classe não tem seu objeto definido e
    referênciado.

## Questão 20

    São palavras reservadas que definem qual é a visibilidade das propieadades de uma classe.
    Private, Protected e Public são os modificadores de acesso.
    Private não permite que nada fora da própia classe acesse a determinada propiedade;
    Protected torna vísivel as classes do mesmo pacote ou atráves de herança;
    Public torna a propiedade acessível a todas as entidades.